function CreateCarousel (options) {
    options = options || {};  
    this.width = options.width || 100;
    this.itemTag = options.itemTag || 'img';
    this.innerElem = options.innerElem || [];
    this.currentIndex = options.currentIndex || 2;
    this.animating = true;
}
CreateCarousel.prototype = {
    createTemplate: function () {
        var wrap = document.createElement('div');
        var item = document.createElement(this.itemTag);
        var template = '';

        item.className = 'item';
        wrap.appendChild(item);
        for(var i = 0; i < this.innerElem.length; i++) {
            item.src = this.innerElem[i];
            template += '<li>' + wrap.innerHTML + '</li>';
        }
        return template;
    },
    navigationDotsTemplate: function () {
        var template = '';

        for(var i = 0; i < this.innerElem.length; i++) {
            if (i === this.currentIndex) {
                template += '<p class="nav-dots active"></p>';
            } else {
                template += '<p class="nav-dots"></p>';
            }
        }
        return template;
    },
    addingTemplate: function () {
        var template = this.createTemplate();
        var dotsTemplate = this.navigationDotsTemplate();

        var skeleton = '<div class="slider">' +
            '<div class="overflow">' +
                '<div class="inner">' +
                    '<ul class="carousel">' +
                        template +
                    '</ul>' +
                '</div>' +
            '</div>' +

            '<p class="carousel-nav prev">' +
                '(‹-)prev' +
            '</p>' +
            dotsTemplate +
            '<p class="carousel-nav next">' +
                'next(-›)' +
            '</p>' +
        '</div>';

        var self = this;
        var start = + (new Date);
        (function waiter () {
            if (document.getElementsByTagName('body').length) {
                var wrapperNode = document.createElement('div');
                var styleNode = document.createElement('style');
                var swipeListener;

                styleNode.setAttribute('type','text/css');
                styleNode.className = 'stale-animate';
                wrapperNode.className = 'carousel-wrap';
                styleNode.innerHTML = self.addingStyle();
                wrapperNode.innerHTML = skeleton;
                document.getElementsByTagName('body')[0].appendChild(styleNode);
                document.getElementsByTagName('body')[0].appendChild(wrapperNode);
                self.mainLogic();
                /**
                 * create instance of slider
                 * @type {CreateCarousel.SwipeControl}
                 */
                swipeListener = new CreateCarousel.SwipeControl({
                    element: document.querySelector('.carousel')
                });
                swipeListener.swipeSuccessCallback = function(dir, dist, time) {
                    if (dist <= 70) {
                        return;
                    }
                    if(dir === 'right'){
                        self.navigationHandler(false);
                    } else {
                        self.navigationHandler(true);
                    }
                };
            } else if(+(new Date) - start < 4000) {
                setTimeout(waiter, 40);
            }
        }());
    },
    addingStyle: function() {
        var prefix = ['-webkit-', '-moz-', '-ms-', '-o-', ''];
        var imgQuantity = this.innerElem.length;
        var styleList =
            '.inner {' +
                'width: ' + 100 * imgQuantity + '%;' +
            '}'+
            '.carousel-wrap {' +
                'width: ' + this.width + '%;' +
                'margin: 10px auto;' +
            '}' +
            '.carousel-nav {' +
                'margin: 5px 5px 0px 0px;' +
                'display: block;' +
                'cursor: pointer;' +
                'display: inline-block;' +
            '}' +
            '.carousel {' +
                'list-style: none;' +
                'width: ' + 100/imgQuantity + '%;' +
            '}' +
            '.carousel > li {' +
                'width: 100%;' +
                'display: block;' +
            '}' +
            '.carousel > li:not(.active) {' +
                'margin-left: -100%;' +
                'float: left;' +
            '}' +
            '.carousel > li.active {' +
                'left: 0px; position: absolute;' +
            '}' +
            '.carousel li img{' +
                'width: 100%;' +
            '}';
        var setPrefix = function(param) {
            return '.carousel > li.prev-in {' +
                param + 'animation: test-prev-in 0.6s ease-in-out 0.0s 1 alternate;' +
            '}' +
            '.carousel > li.next-in {' +
                param + 'animation: test-next-in 0.6s ease-in-out 0.0s 1 alternate;' +
            '}' +
            '.carousel > li.next-out {' +
                param + 'animation: test-next-out 0.6s ease-in-out 0.0s 1 alternate;' +
            '}' +
            '.carousel > li.prev-out {' +
                param + 'animation: test-prev-out 0.6s ease-in-out 0.0s 1 alternate;' +
            '}' +
            '@' + param + 'keyframes test-prev-in {' +
                '0% { left: -100%; }' +
                '100% { left: 0px; }' +
            '}' +
            '@' + param + 'keyframes test-next-in {' +
                '0% { left: 100%; }' +
                '100% { left: 0px; }' +
            '}' +
            '@' + param + 'keyframes test-next-out {' +
                '0% { left: 0px; }' +
                '100% { left: -100%; }' +
            '}' +
            '@'+ param +'keyframes test-prev-out {' +
                '0% { left: 0px; }' +
                '100% { left: 100%; }' +
            '}';
        };
        for(var i = 0; i < prefix.length; i++) {
            styleList += setPrefix(prefix[i])
        }
        return styleList;
    },
    helperFunctions: {
        addClass: function (o, c) {
            var re = new RegExp("(^|\\s)" + c + "(\\s|$)", "g");
            if (re.test(o.className)){
              return;
            }
            o.className = (o.className + " " + c).replace(/\s+/g, " ").replace(/(^ | $)/g, "")
        },
        removeClass: function (o, c) {
            var re;
            var removeLogic = function(classElem){
                re = new RegExp("(^|\\s)" + classElem + "(\\s|$)", "g");
                o.className = o.className.replace(re, "$1").replace(/\s+/g, " ").replace(/(^ | $)/g, "");
            };
            if(Array.isArray(c)){
                for(var i = 0; i < c.length; i++){
                    removeLogic(c[i]);
                }
            } else {
                removeLogic(c);
            }
        },
        hasClass: function(el, selector) {
            var className = " " + selector + " ";
            if ((" " + el.className + " ").replace(/[\n\t]/g, " ").indexOf(className) > -1) {
                return true;
            }
            return false;
        },
        addEvent: function(elem, evType, fn) {
            if (elem.addEventListener) {
              elem.addEventListener(evType, fn, false);
            }
            else if (elem.attachEvent) {
              elem.attachEvent('on' + evType, fn)
            }
            else {
              elem['on' + evType] = fn
            }
        }

    },
    mainLogic: function () {
        var arrayOfItems = document.querySelectorAll('.carousel > li');
        var navigation = document.querySelectorAll('.carousel-nav');
        var navigationDots = document.querySelectorAll('.nav-dots');
        var self = this;

        var dotsClick = function (i, quantity) {
            self.helperFunctions.addEvent(navigationDots[i], 'click', function () {
                if (self.helperFunctions.hasClass(this, 'active')) {
                    return;
                }

                self.navigationHandler(false, i, quantity);
            });
        };
        /**
         * add the active class
         */
        if (arrayOfItems.length) {
            self.helperFunctions.addClass(arrayOfItems[self.currentIndex], 'active');
        }
        /**
         * set handlers for next and prev button
         */
        for(var i = 0; i < navigation.length; i++) {
            self.helperFunctions.addEvent(navigation[i], 'click', function () {
                if(self.helperFunctions.hasClass(this, 'next')){
                    self.navigationHandler(true);
                } else {
                    self.navigationHandler(false);
                }
            });
        }
        for(var i = 0; i < navigationDots.length; i++) {
            dotsClick(i, navigationDots.length);
        }
    },
    changeActiveBottomDot: function (elem, index) {
        this.helperFunctions.removeClass(elem[0], 'active');
        this.helperFunctions.addClass(document.querySelectorAll('.nav-dots')[index], 'active');
    },
    navigationHandler: function (isNext, index, quantity) {
        var activeItem  = document.querySelectorAll('.carousel > li.active');
        var animatedElem = activeItem[0];
        var pfx = ["webkit", "moz", "MS", "o", ""];
        var nextItem;
        var self = this;
        var itemCount = this.innerElem.length;

        if(!this.animating) {
            return false;
        }


        if (index || index === 0) {

            isNext = (this.currentIndex < index);

            this.currentIndex = index;
        } else {
            this.currentIndex = (this.currentIndex + (isNext ? 1 : -1)) % itemCount;

            /**
             * go back to the last item if we hit -1
             */

            if (this.currentIndex === -1) {
                this.currentIndex = itemCount - 1;
            }
        }

        if (document.querySelectorAll('.nav-dots.active')[0]) {
            this.changeActiveBottomDot(document.querySelectorAll('.nav-dots.active'), this.currentIndex);
        }
        nextItem = document.querySelectorAll('.carousel > li')[self.currentIndex];
        this.helperFunctions.addClass(animatedElem, isNext ? 'next-out' : 'prev-out');

        this.helperFunctions.addClass(nextItem, 'active');
        this.helperFunctions.addClass(nextItem, isNext ? 'next-in' : 'prev-in');

        /**
         * remove handle animation event
         */
        var removeEvent =  function(element, type, callback) {
          for (var p = 0; p < pfx.length; p++) {
            if (!pfx[p]) type = type.toLowerCase();
            element.removeEventListener(pfx[p]+type, callback, false);
          }
        };
        /**
         * handle animation events
         */
        var animationListener = function(e) {
            if (e.type.toLowerCase().indexOf("animationend") >= 0) {
                self.animating = true;
                self.helperFunctions.removeClass(animatedElem, ['active','next-out','prev-out']);
                self.helperFunctions.removeClass(nextItem, ['next-in','prev-in']);
                removeEvent(animatedElem, "AnimationEnd", animationListener);
            }
        };
        /**
         * apply prefixed event handlers
         */
        var prefixedEvent = function(element, type, callback) {
          for (var p = 0; p < pfx.length; p++) {
            if (!pfx[p]) type = type.toLowerCase();
            element.addEventListener(pfx[p]+type, callback, false);
          }
        };

        /**
         * animation listener events
         */
        prefixedEvent(animatedElem, "AnimationEnd", animationListener);

        this.animating = false;
    }
};
CreateCarousel.SwipeControl = function (opts) {
    this.opts = opts;
    this._init(opts);
};
CreateCarousel.SwipeControl.prototype.setListeners = function () {
    var self = this,
        hasTouch = 'ontouchstart' in window,
        startEvent = hasTouch ? 'touchstart' : 'mousedown',
        moveEvent = hasTouch ? 'touchmove' : 'mousemove',
        endEvent = hasTouch ? 'touchend' : 'mouseup';
    // Set the touch start event to track where the swipe originated.
    this.element.addEventListener(startEvent, function (e) {
        if (e.targetTouches && 1 === e.targetTouches.length || 'mousedown' === e.type) {
            var eventObj = hasTouch ? e.targetTouches[0] : e;
            // Set the start event related properties.
            self.startTime = Date.now();
            self.start = parseInt(eventObj.pageX);
        }
        // e.preventDefault();
    }, false);
    // Set the touch move event to track the swipe movement.
    this.element.addEventListener(moveEvent, function (e) {
        if (self.start && (e.targetTouches && 1 === e.targetTouches.length || 'mousemove' === e.type)) {
            var eventObj = hasTouch ? e.targetTouches[0] : e;
            // Set the current position related properties.
            self.currTime = Date.now();
            self.currPos = parseInt(eventObj.pageX);
            self.trackSwipe();
        }
        e.preventDefault();
    }, false);
    // Set the touch end event to track where the swipe finished.
    this.element.addEventListener(endEvent, function (e) {
        var eventObj = hasTouch ? e.changedTouches[0] : e;
        // Set the end event related properties.
        self.endTime = Date.now();
        self.end = parseInt(eventObj.pageX);
        // Run the confirm swipe method.
        self.confirmSwipe();
        // e.preventDefault();
    }, false);
};
CreateCarousel.SwipeControl.prototype.trackSwipe = function () {
    var self = this;
    // Overwrite the function properties.
    this.direction = (this.start > this.currPos) ? 'left' : 'right';
    this.trackDistance = ('left' === this.direction) ? (this.start - this.currPos) : (this.currPos - this.start);
    // Run the tracking callback.
    this.trackingCallback(this.direction, this.trackDistance, this.currPos, this.start, parseInt(this.currTime - this.startTime));
};
CreateCarousel.SwipeControl.prototype.confirmSwipe = function () {
    //var self = this;
    // Set up the direction property.
    this.direction = (this.start > this.end) ? 'left' : 'right';
    // Set up the duration property.
    this.duration = parseInt(this.endTime - this.startTime);
    // Work out the distance based on the direction of the swipe.
    this.swipeDistance = ('left' === this.direction) ? (this.start - this.end) : (this.end - this.start);
    // This is where we determine whether it was a swipe or not.
    this.swipeSuccessCallback(this.direction, this.swipeDistance, this.duration);
    // Reset the variables.
    this._config();
};
CreateCarousel.SwipeControl.prototype._config = function () {
    this.start = null;
    this.end = null;
    this.trackDistance = null;
    this.swipeDistance = null;
    this.currPos = null;
    this.startTime = null;
    this.endTime = null;
    this.currTime = null;
    this.direction = null;
    this.duration = null;
};
CreateCarousel.SwipeControl.prototype._init = function (opts) {
    // Set the function properties.
    this.element = this.opts.element;
    this.swipeSuccessCallback = this.opts.swipeSuccessCallback || function (dir, dist, time) {
        //console.log('SuccessCallback', dir, dist, time);
        //console.log('SuccessCallback', dir);
    };
    this.trackingCallback = this.opts.trackingCallback || function (dir, dist, currPos, time) {
        //console.log('trackingCallback', dir, dist, currPos, time);
    };
    // Run the function methods.
    this._config();
    this.setListeners();
    return this;
};
carousel = new CreateCarousel({
    'width': '95',
    'currentIndex': 1,
    'innerElem': ['images/img_1.jpg', 'images/img_2.jpg', 'images/img_3.jpg', 'images/img_4.jpg', 'images/img_5.jpg']
});
carousel.addingTemplate();